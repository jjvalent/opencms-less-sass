/* ***************************************************************************
 * This library is part of INFONOVA OpenCms Modules.
 * Common Modules for the Open Source Content Management System: OpenCms.
 * 
 * Copyright (c) 2010 INFONOVA GmbH, Austria.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * For further information about INFONOVA GmbH, Austria, please
 * see the company website: http://www.infonova.at/
 *
 * For further information about INFONOVA OpenCms Modules, please see the
 * project website: http://sourceforge.net/projects/bp-cms-commons/
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *****************************************************************************/
package com.dispatch.optimizer;

import java.util.Set;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.test.OpenCmsTestCase;
import org.opencms.test.OpenCmsTestProperties;

//import com.dispatch.framework.OpenCmsModuleTest;

/**
 * Tests the OpenCms system roles.
 * <p>
 */
public class OpenCmsModuleTest extends OpenCmsTestCase {

	private static final Log LOG = CmsLog.getLog(OpenCmsModuleTest.class);

	/**
	 * Default JUnit constructor.
	 * <p>
	 * 
	 * @param arg0
	 *            JUnit parameters
	 */
	public OpenCmsModuleTest(final String arg0) {
		super(arg0);
	}

	/**
	 * Test suite for this test class.
	 * 
	 * @return the test suite
	 */
	public static Test suite() {
		OpenCmsTestProperties
				.initialize(org.opencms.test.AllTests.TEST_PROPERTIES_PATH);

		TestSuite suite = new TestSuite();
		suite.setName(OpenCmsModuleTest.class.getName());

		suite.addTest(new OpenCmsModuleTest("testModuleImport"));

		TestSetup wrapper = new TestSetup(suite) {

			@Override
			protected void setUp() {
				setupOpenCms("xmlcontent", "/");
			}

			@Override
			protected void tearDown() {
				removeOpenCms();
			}
		};

		return wrapper;
	}

	/**
	 * <p>
	 * Simple Test if the Module 'at.infonova.opencms.modules.demo.content5' is
	 * imported into the OpenCms-Test instance.
	 * 
	 * <p>
	 * see src\test\config\WEB-INF\config\opencms-modules.xml
	 * 
	 */
	public void testModuleImport() {
		echo("Testing testModuleImport");

		String projectModuleName = "com.dispatch.optimizer";

		// basic check if the module was imported correctly (during
		// configuration)
		if (!OpenCms.getModuleManager().hasModule(projectModuleName)) {

			Set<String> moduleNames = getModuleNames();
			for (String moduleName : moduleNames) {
				LOG.info("imported Module: " + moduleName);
			}
			fail("Module '" + projectModuleName + "' was not imported!");
		}
	}

	/**
	 * Generic Version of
	 * {@link org.opencms.module.CmsModuleManager#getModuleNames()}.
	 * 
	 * @return Generic Version of return-Value
	 *         {@link org.opencms.module.CmsModuleManager#getModuleNames()}
	 */
	private Set<String> getModuleNames() {
		return OpenCms.getModuleManager().getModuleNames();
	}
}
