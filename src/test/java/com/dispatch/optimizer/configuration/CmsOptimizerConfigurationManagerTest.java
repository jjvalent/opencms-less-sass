/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.configuration;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.opencms.test.OpenCmsTestCase;
import org.opencms.test.OpenCmsTestProperties;

import com.dispatch.optimizer.configurartion.CmsOptimizerConfigurationManager;

public class CmsOptimizerConfigurationManagerTest extends OpenCmsTestCase {

  /** The expected encoding from the configuration file */
  private static final String EXPECTED_ENCODING = "UTF-8";

  public CmsOptimizerConfigurationManagerTest(String arg0) {
    super(arg0);
  }

  /**
   * Test suite for this test class.
   * 
   * @return the test suite
   */
  public static Test suite() {
    OpenCmsTestProperties
        .initialize(org.opencms.test.AllTests.TEST_PROPERTIES_PATH);

    TestSuite suite = new TestSuite();
    suite.setName(CmsOptimizerConfigurationManagerTest.class.getName());

    suite.addTest(new CmsOptimizerConfigurationManagerTest(
        "testCmsOptimizerConfigurationNotNull"));
    suite.addTest(new CmsOptimizerConfigurationManagerTest(
        "testCmsOptimizerConfigurationMapSize5"));
    suite.addTest(new CmsOptimizerConfigurationManagerTest(
        "testCmsOptimizerConfigurationContainsLessCss"));
    suite.addTest(new CmsOptimizerConfigurationManagerTest(
        "testCmsOptimizerConfigurationIgnoreMissingResourcesTrue"));
    suite.addTest(new CmsOptimizerConfigurationManagerTest(
        "testCmsOptimizerConfigurationEncodingEquals"));

    TestSetup wrapper = new TestSetup(suite) {

      @Override
      protected void setUp() {
        setupOpenCms("xmlcontent", "/");
      }

      @Override
      protected void tearDown() {
        removeOpenCms();
      }
    };

    return wrapper;
  }

  public void testCmsOptimizerConfigurationNotNull() {
    echo("Test testCmsOptimizerConfigurationNotNull beginning...");
    assertNotNull(CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration());
    echo("Test testCmsOptimizerConfigurationNotNull ending...");
  }

  public void testCmsOptimizerConfigurationMapSize5() {
    echo("Test testCmsOptimizerConfigurationMapSize5 beginning...");
    assertEquals(5, CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration().getProcessorMap().size());
    echo("Test testCmsOptimizerConfigurationMapSize5 ending...");
  }

  public void testCmsOptimizerConfigurationContainsLessCss() {
    echo("Test testCmsOptimizerConfigurationContainsLessCss beginning...");
    assertTrue(CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration().getProcessorMap()
        .containsKey("lessCss"));
    echo("Test testCmsOptimizerConfigurationContainsLessCss ending...");
  }

  public void testCmsOptimizerConfigurationIgnoreMissingResourcesTrue() {
    echo("Test testCmsOptimizerConfigurationIgnoreMissingResourcesTrue beginning...");
    // SET IT TO TRUE SINCE IT IS GETTING OVERRIDDEN
    CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration().setIgnoreMissingResources(true);
    assertTrue(CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration().isIgnoreMissingResources());
    echo("Test testCmsOptimizerConfigurationIgnoreMissingResourcesTrue ending...");
  }

  public void testCmsOptimizerConfigurationEncodingEquals() {
    echo("Test testCmsOptimizerConfigurationEncodingEquals beginning...");
    assertEquals("Encoding should be equals to: " + EXPECTED_ENCODING,
        EXPECTED_ENCODING, CmsOptimizerConfigurationManager
            .getInstance().getOptimizerConfiguration()
            .getEncoding());
    echo("Test testCmsOptimizerConfigurationEncodingEquals ending...");
  }
}
