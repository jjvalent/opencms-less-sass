/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dispatch.optimizer.processor;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.opencms.jsp.CmsJspActionElement;
import org.opencms.test.OpenCmsTestCase;
import org.opencms.test.OpenCmsTestProperties;
import org.opencms.workplace.CmsDialog;

import at.infonova.opencms.testing.CmsJspActionElementFactory;

/**
 * @author jvalentine
 * 
 */
public class CmsWebResourceOptimizerProcessorTest extends OpenCmsTestCase {

  /** The {@link CmsWebResourceOptimizerProcessor} to test */
  private CmsWebResourceOptimizerProcessor m_webResourceProcessor;

  /** The jsp resource to use for testing */
  private static final String RESOURCE_URI = "/system/workplace/commons/optimizer-web-resource.jsp";

  /** The param resource uri to test against */
  private static final String PARAM_RESOURCE_URI = "/css/dispatch.less";

  /** The param resource uri for scss to test against */
  private static final String PARAM_RESOURCE_URI_SCSS = "/css/sass/dispatch.scss";

  /**
   * The param resource for test.less that is using the property generated
   * name to test against
   */
  private static final String PARAM_RESOURCE_TEST_LESS = "/css/test.less";

  /**
   * The param resource for recursive.less that is using the property
   * processors to test against
   */
  private static final String PARAM_RESOURCE_RECURSIVE_LESS = "/css/recursive.less";

  /** The param resource uri that is a folder to test against */
  private static final String PARAM_RESOURCE_FOLDER_URI = "/";

  /**
   * @param arg0
   */
  public CmsWebResourceOptimizerProcessorTest(String arg0) {
    super(arg0);
  }

  /**
   * @param arg0
   * @param initialize
   */
  public CmsWebResourceOptimizerProcessorTest(String arg0, boolean initialize) {
    super(arg0, initialize);
  }

  @SuppressWarnings("unchecked")
  public void setUp() throws Exception {

    // CREATE A NEW JSP ACTION ELEMENT
    CmsJspActionElement jspActionElement = CmsJspActionElementFactory
        .createCmsJspActionElement(RESOURCE_URI, "Admin", "admin");

    // SET THE JSP ACTION ELEMENT TO USE OFFLINE
    jspActionElement
        .getCmsObject()
        .getRequestContext()
        .setCurrentProject(
            jspActionElement.getCmsObject().readProject("Offline"));

    // ADD THE PARAM RESOURCE TO THE PARAM MAP
    jspActionElement
        .getRequest()
        .getParameterMap()
        .put(CmsDialog.PARAM_RESOURCE,
            new String[] { PARAM_RESOURCE_URI });

    // CREATE A NEW WEB RESOURCE PROCESSOR FROM THE ACTION ELEMENT
    setWebResourceProcessor(new CmsWebResourceOptimizerProcessor(
        jspActionElement));
  }

  /**
   * Test suite for this test class.
   * 
   * @return the test suite
   */
  public static Test suite() {
    OpenCmsTestProperties
        .initialize(org.opencms.test.AllTests.TEST_PROPERTIES_PATH);

    TestSuite suite = new TestSuite();
    suite.setName(CmsWebResourceOptimizerProcessorTest.class.getName());

    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorNotNull"));
    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorActionBuildTrue"));
    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorForwarded_False"));
    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorFolder_False"));
    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorScss_True"));
    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorGeneratedName_True"));
    suite.addTest(new CmsWebResourceOptimizerProcessorTest(
        "testCmsWebResourceOptimizerProcessorRecursive_True"));

    TestSetup wrapper = new TestSetup(suite) {

      @Override
      protected void setUp() {
        setupOpenCms("xmlcontent", "/");
      }

      @Override
      protected void tearDown() {
        removeOpenCms();
      }
    };

    return wrapper;
  }

  public void testCmsWebResourceOptimizerProcessorNotNull() {
    assertNotNull(getWebResourceProcessor());
  }

  public void testCmsWebResourceOptimizerProcessorActionBuildTrue()
      throws Exception {
    boolean operationSuccessful = getWebResourceProcessor().actionBuild();

    assertTrue("The perform dialog should have returned true",
        operationSuccessful);
  }

  public void testCmsWebResourceOptimizerProcessorForwarded_False()
      throws Exception {
    // SET FORWARDED TO TRUE
    getWebResourceProcessor().setForwarded(true);
    boolean operationSuccessful = getWebResourceProcessor().actionBuild();

    assertFalse(
        "The perform dialog should have returned false since it was forwarded",
        operationSuccessful);
  }

  @SuppressWarnings("unchecked")
  public void testCmsWebResourceOptimizerProcessorFolder_False()
      throws Exception {
    // CREATE A NEW JSP ACTION ELEMENT
    CmsJspActionElement jspActionElement = CmsJspActionElementFactory
        .createCmsJspActionElement(RESOURCE_URI, "Admin", "admin");

    // SET THE JSP ACTION ELEMENT TO USE OFFLINE
    jspActionElement
        .getCmsObject()
        .getRequestContext()
        .setCurrentProject(
            jspActionElement.getCmsObject().readProject("Offline"));

    // ADD THE PARAM RESOURCE TO THE PARAM MAP
    jspActionElement
        .getRequest()
        .getParameterMap()
        .put(CmsDialog.PARAM_RESOURCE,
            new String[] { PARAM_RESOURCE_FOLDER_URI });

    // CREATE A NEW WEB RESOURCE PROCESSOR FROM THE ACTION ELEMENT
    setWebResourceProcessor(new CmsWebResourceOptimizerProcessor(
        jspActionElement));

    boolean operationSuccessful = getWebResourceProcessor().actionBuild();

    assertFalse(
        "The perform dialog should have returned false since the resource is a folder",
        operationSuccessful);
  }

  @SuppressWarnings("unchecked")
  public void testCmsWebResourceOptimizerProcessorScss_True()
      throws Exception {
    // CREATE A NEW JSP ACTION ELEMENT
    CmsJspActionElement jspActionElement = CmsJspActionElementFactory
        .createCmsJspActionElement(RESOURCE_URI, "Admin", "admin");

    // SET THE JSP ACTION ELEMENT TO USE OFFLINE
    jspActionElement
        .getCmsObject()
        .getRequestContext()
        .setCurrentProject(
            jspActionElement.getCmsObject().readProject("Offline"));

    // ADD THE PARAM RESOURCE TO THE PARAM MAP
    jspActionElement
        .getRequest()
        .getParameterMap()
        .put(CmsDialog.PARAM_RESOURCE,
            new String[] { PARAM_RESOURCE_URI_SCSS });

    // CREATE A NEW WEB RESOURCE PROCESSOR FROM THE ACTION ELEMENT
    setWebResourceProcessor(new CmsWebResourceOptimizerProcessor(
        jspActionElement.getJspContext(),
        jspActionElement.getRequest(), jspActionElement.getResponse()));

    boolean operationSuccessful = getWebResourceProcessor().actionBuild();

    assertTrue(
        "The perform dialog should have returned true, we processed a sass file",
        operationSuccessful);
  }

  @SuppressWarnings("unchecked")
  public void testCmsWebResourceOptimizerProcessorGeneratedName_True()
      throws Exception {
    // CREATE A NEW JSP ACTION ELEMENT
    CmsJspActionElement jspActionElement = CmsJspActionElementFactory
        .createCmsJspActionElement(RESOURCE_URI, "Admin", "admin");

    // SET THE JSP ACTION ELEMENT TO USE OFFLINE
    jspActionElement
        .getCmsObject()
        .getRequestContext()
        .setCurrentProject(
            jspActionElement.getCmsObject().readProject("Offline"));

    // ADD THE PARAM RESOURCE TO THE PARAM MAP
    jspActionElement
        .getRequest()
        .getParameterMap()
        .put(CmsDialog.PARAM_RESOURCE,
            new String[] { PARAM_RESOURCE_TEST_LESS });

    // CREATE A NEW WEB RESOURCE PROCESSOR FROM THE ACTION ELEMENT
    setWebResourceProcessor(new CmsWebResourceOptimizerProcessor(
        jspActionElement.getJspContext(),
        jspActionElement.getRequest(), jspActionElement.getResponse()));

    boolean operationSuccessful = getWebResourceProcessor().actionBuild();

    assertTrue(
        "The perform dialog should have returned true, we processed a less file with a generated name",
        operationSuccessful);
  }

  @SuppressWarnings("unchecked")
  public void testCmsWebResourceOptimizerProcessorRecursive_True()
      throws Exception {
    // CREATE A NEW JSP ACTION ELEMENT
    CmsJspActionElement jspActionElement = CmsJspActionElementFactory
        .createCmsJspActionElement(RESOURCE_URI, "Admin", "admin");

    // SET THE JSP ACTION ELEMENT TO USE OFFLINE
    jspActionElement
        .getCmsObject()
        .getRequestContext()
        .setCurrentProject(
            jspActionElement.getCmsObject().readProject("Offline"));

    // ADD THE PARAM RESOURCE TO THE PARAM MAP
    jspActionElement
        .getRequest()
        .getParameterMap()
        .put(CmsDialog.PARAM_RESOURCE,
            new String[] { PARAM_RESOURCE_RECURSIVE_LESS });

    // CREATE A NEW WEB RESOURCE PROCESSOR FROM THE ACTION ELEMENT
    setWebResourceProcessor(new CmsWebResourceOptimizerProcessor(
        jspActionElement.getJspContext(),
        jspActionElement.getRequest(), jspActionElement.getResponse()));

    boolean operationSuccessful = getWebResourceProcessor().actionBuild();

    assertTrue(
        "The perform dialog should have returned true, we processed a less file with a defined list of processors",
        operationSuccessful);
  }

  /**
   * @return the m_webResourceProcessor
   */
  public CmsWebResourceOptimizerProcessor getWebResourceProcessor() {
    return m_webResourceProcessor;
  }

  /**
   * @param m_webResourceProcessor
   *            the m_webResourceProcessor to set
   */
  public void setWebResourceProcessor(
      CmsWebResourceOptimizerProcessor m_webResourceProcessor) {
    this.m_webResourceProcessor = m_webResourceProcessor;
  }

}
