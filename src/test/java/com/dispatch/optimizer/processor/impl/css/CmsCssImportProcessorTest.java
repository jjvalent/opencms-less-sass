/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor.impl.css;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.opencms.test.OpenCmsTestCase;
import org.opencms.test.OpenCmsTestLogAppender;
import org.opencms.test.OpenCmsTestProperties;

import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;
import ro.isdc.wro.util.WroTestUtils;

import com.dispatch.optimizer.configurartion.CmsOptimizerConfigurationManager;

public class CmsCssImportProcessorTest extends OpenCmsTestCase {

  /** The {@link ResourcePreProcessor} processor we are testing against */
  private ResourcePreProcessor processor;

  /** The URI for this test */
  private static final String RESOURCE_URI = "/css/dispatch.less";

  /** The Recusrive URI for this test */
  private static final String RECURSIVE_RESOURCE_URI = "/css/recursive.less";

  public CmsCssImportProcessorTest(String arg0) {
    super(arg0);
  }

  public void setUp() throws Exception {
    final WroConfiguration config = new WroConfiguration();
    config.setIgnoreFailingProcessor(CmsOptimizerConfigurationManager
        .getInstance().getOptimizerConfiguration()
        .isIgnoreMissingResources());
    Context.set(Context.standaloneContext(), config);
    processor = new CmsCssImportProcessor(getCmsObject());
    WroTestUtils.initProcessor(processor);
  }

  /**
   * Test suite for this test class.
   * 
   * @return the test suite
   */
  public static Test suite() {
    OpenCmsTestProperties
        .initialize(org.opencms.test.AllTests.TEST_PROPERTIES_PATH);

    TestSuite suite = new TestSuite();
    suite.setName(CmsCssImportProcessorTest.class.getName());

    suite.addTest(new CmsCssImportProcessorTest(
        "shouldSupportCorrectResourceTypes"));
    suite.addTest(new CmsCssImportProcessorTest(
        "shouldNotFailWhenInvalidResourceIsFound"));
    suite.addTest(new CmsCssImportProcessorTest(
        "shouldFailWhenInvalidResourceIsFound"));
    suite.addTest(new CmsCssImportProcessorTest("testProcessCssImportFile"));
    suite.addTest(new CmsCssImportProcessorTest(
        "testProcessCssImportFileIgnoreRecusrionFiles"));

    TestSetup wrapper = new TestSetup(suite) {

      @Override
      protected void setUp() {
        setupOpenCms("xmlcontent", "/");
      }

      @Override
      protected void tearDown() {
        removeOpenCms();
      }
    };

    return wrapper;
  }

  /**
   * Make sure the processor supports CSS
   */
  public void shouldSupportCorrectResourceTypes() {
    WroTestUtils.assertProcessorSupportResourceTypes(processor,
        ResourceType.CSS);
  }

  public void shouldNotFailWhenInvalidResourceIsFound() throws Exception {
    CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration().setIgnoreMissingResources(true);
    processInvalidImport();
  }

  public void shouldFailWhenInvalidResourceIsFound() {
    // NEEDED SINCE A IO EXCEPTION IS THROWN
    OpenCmsTestLogAppender.setBreakOnError(false);
    try {
      CmsOptimizerConfigurationManager.getInstance()
          .getOptimizerConfiguration()
          .setIgnoreMissingResources(false);
      processInvalidImport();

      fail("This code should have thrown an IO Exception");
    } catch (IOException ex) {
      // NOOP
    }
  }

  public void testProcessCssImportFile() throws Exception {
    // CREATE A NEW RESOURCE
    final Resource resource = Resource.create(RESOURCE_URI,
        ResourceType.CSS);

    final Reader reader = new StringReader(new String(getCmsObject()
        .readFile(RESOURCE_URI).getContents()));

    StringWriter writer = new StringWriter();
    processor.process(resource, reader, writer);

    final String generatedCss = writer.toString();

    assertNotNull(writer);
    assertTrue(generatedCss.contains("body {"));
  }

  public void testProcessCssImportFileIgnoreRecusrionFiles() throws Exception {
    // CREATE A NEW RESOURCE
    final Resource resource = Resource.create(RECURSIVE_RESOURCE_URI,
        ResourceType.CSS);

    final Reader reader = new StringReader(new String(getCmsObject()
        .readFile(RECURSIVE_RESOURCE_URI).getContents()));

    StringWriter writer = new StringWriter();
    processor.process(resource, reader, writer);

    final String generatedCss = writer.toString();

    assertNotNull(writer);
    assertTrue(generatedCss.contains("body {"));
  }

  private void processInvalidImport() throws IOException {
    final Resource resource = Resource.create("someResource.css");
    final Reader reader = new StringReader(
        "@import('/path/to/invalid.css');");
    processor.process(resource, reader, new StringWriter());
  }
}
