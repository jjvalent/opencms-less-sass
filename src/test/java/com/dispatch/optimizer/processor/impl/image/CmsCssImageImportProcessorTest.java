/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor.impl.image;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.opencms.test.OpenCmsTestCase;
import org.opencms.test.OpenCmsTestLogAppender;
import org.opencms.test.OpenCmsTestProperties;

import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;
import ro.isdc.wro.util.WroTestUtils;

import com.dispatch.optimizer.configurartion.CmsOptimizerConfigurationManager;

/**
 * @author John Valentine
 * 
 */
public class CmsCssImageImportProcessorTest extends OpenCmsTestCase {

  /** The {@link ResourcePreProcessor} processor we are testing against */
  private ResourcePreProcessor m_processor;

  /** The URI for this test */
  private static final String IMAGE_CSS_RESOURCE_URI = "/digital/css/images.css";

  /**
   * @param arg0
   */
  public CmsCssImageImportProcessorTest(String arg0) {
    super(arg0);
  }

  /**
   * @param arg0
   * @param initialize
   */
  public CmsCssImageImportProcessorTest(String arg0, boolean initialize) {
    super(arg0, initialize);
  }

  public void setUp() throws Exception {
    final WroConfiguration config = new WroConfiguration();
    config.setIgnoreFailingProcessor(CmsOptimizerConfigurationManager
        .getInstance().getOptimizerConfiguration()
        .isIgnoreMissingResources());
    Context.set(Context.standaloneContext(), config);
    setProcessor(new CmsCssImageImportProcessor(getCmsObject()));
    WroTestUtils.initProcessor(getProcessor());
  }

  /**
   * Test suite for this test class.
   * 
   * @return the test suite
   */
  public static Test suite() {
    OpenCmsTestProperties
        .initialize(org.opencms.test.AllTests.TEST_PROPERTIES_PATH);

    TestSuite suite = new TestSuite();
    suite.setName(CmsCssImageImportProcessorTest.class.getName());

    suite.addTest(new CmsCssImageImportProcessorTest(
        "shouldSupportCorrectResourceTypes"));
    suite.addTest(new CmsCssImageImportProcessorTest(
        "shouldNotFailWhenInvalidResourceIsFound"));
    suite.addTest(new CmsCssImageImportProcessorTest(
        "shouldFailWhenInvalidResourceIsFound"));
    suite.addTest(new CmsCssImageImportProcessorTest(
        "testProcessCssImportFile"));

    TestSetup wrapper = new TestSetup(suite) {

      @Override
      protected void setUp() {
        setupOpenCms("xmlcontent", "/");
      }

      @Override
      protected void tearDown() {
        removeOpenCms();
      }
    };

    return wrapper;
  }

  /**
   * Make sure the processor supports CSS
   */
  public void shouldSupportCorrectResourceTypes() {
    WroTestUtils.assertProcessorSupportResourceTypes(getProcessor(),
        ResourceType.CSS);
  }

  public void shouldNotFailWhenInvalidResourceIsFound() throws Exception {
    CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration().setIgnoreMissingResources(true);
    processInvalidImport();
  }

  public void shouldFailWhenInvalidResourceIsFound() {
    // NEEDED SINCE A IO EXCEPTION IS THROWN
    OpenCmsTestLogAppender.setBreakOnError(false);
    try {
      CmsOptimizerConfigurationManager.getInstance()
          .getOptimizerConfiguration()
          .setIgnoreMissingResources(false);
      processInvalidImport();

      fail("This code should have thrown an IO Exception");
    } catch (IOException ex) {
      // NOOP
    }
  }

  public void testProcessCssImportFile() throws Exception {
    // CREATE A NEW RESOURCE
    final Resource resource = Resource.create(IMAGE_CSS_RESOURCE_URI,
        ResourceType.CSS);

    final Reader reader = new StringReader(new String(getCmsObject()
        .readFile(IMAGE_CSS_RESOURCE_URI).getContents()));

    StringWriter writer = new StringWriter();
    getProcessor().process(resource, reader, writer);

    final String generatedCss = writer.toString();

    assertNotNull(writer);
    assertTrue(generatedCss.contains("url"));
    assertTrue(generatedCss.contains("no-repeat 7px 0;"));
  }

  private void processInvalidImport() throws IOException {
    final Resource resource = Resource.create("someResource.css");
    final Reader reader = new StringReader(
        "background-image: url( \"path/to/image.png\" );");
    getProcessor().process(resource, reader, new StringWriter());
  }

  /**
   * @return the processor
   */
  public ResourcePreProcessor getProcessor() {
    return m_processor;
  }

  /**
   * @param processor
   *            the processor to set
   */
  public void setProcessor(ResourcePreProcessor processor) {
    this.m_processor = processor;
  }

}
