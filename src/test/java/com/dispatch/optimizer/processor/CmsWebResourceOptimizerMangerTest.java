/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dispatch.optimizer.processor;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.commons.lang3.StringUtils;
import org.opencms.file.CmsResource;
import org.opencms.test.OpenCmsTestCase;
import org.opencms.test.OpenCmsTestProperties;

import ro.isdc.wro.extensions.processor.css.LessCssProcessor;
import ro.isdc.wro.extensions.processor.css.RubySassCssProcessor;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;

import com.dispatch.optimizer.processor.impl.css.CmsCssImportProcessor;

/**
 * Tests the {@link CmsWebResourceOptimizerManger}
 * 
 * @author John Valentine
 * 
 */
public class CmsWebResourceOptimizerMangerTest extends OpenCmsTestCase {

  /** The {@link CmsWebResourceOptimizerManager} we are testing against */
  private CmsWebResourceOptimizerManger m_manager;

  /** The friendly name of the less css processor */
  private static final String FRIENDLY_NAME_LESS_PROCESSOR = "lessCss";

  /** The friendly name of the sass css processor */
  private static final String FRIENDLY_NAME_SASS_PROCESSOR = "sassCss";

  /** The URI for this test */
  private static final String RESOURCE_URI = "/css/dispatch.less";

  /** The URI for this test */
  private static final String SASS_RESOURCE_URI = "/css/sass/dispatch.scss";

  /** The URI to write for this test */
  private static final String WRITE_RESOURCE_URI = "/css/dispatch.css";

  /**
   * @param arg0
   *            the name of the test(method in this class) to run
   */
  public CmsWebResourceOptimizerMangerTest(String arg0) {
    super(arg0);
  }

  public void setUp() {
    setManager(CmsWebResourceOptimizerManger.getInstance());
  }

  /**
   * Test suite for this test class.
   * 
   * @return the test suite
   */
  public static Test suite() {
    OpenCmsTestProperties
        .initialize(org.opencms.test.AllTests.TEST_PROPERTIES_PATH);

    TestSuite suite = new TestSuite();
    suite.setName(CmsWebResourceOptimizerMangerTest.class.getName());

    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testManagerNotNull"));
    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testCmsCssImportProcessorLoaded"));
    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testLessCssProcessorLoaded"));
    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testSassCssProcessorLoaded"));
    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testCssFileCreated"));
    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testLessCssProcessorExecuted"));
    suite.addTest(new CmsWebResourceOptimizerMangerTest(
        "testSassCssProcessorExecuted"));

    TestSetup wrapper = new TestSetup(suite) {

      @Override
      protected void setUp() {
        setupOpenCms("xmlcontent", "/");
      }

      @Override
      protected void tearDown() {
        removeOpenCms();
      }
    };

    return wrapper;
  }

  public void testManagerNotNull() {
    assertNotNull(getManager());
  }

  public void testCmsCssImportProcessorLoaded() throws Exception {
    ResourcePreProcessor processor = getManager().getResourcePreProccessor(
        getCmsObject(), CmsCssImportProcessor.FRIENDLY_NAME);

    assertNotNull(processor);
    assertTrue(processor instanceof CmsCssImportProcessor);
  }

  public void testLessCssProcessorLoaded() throws Exception {
    ResourcePreProcessor processor = getManager().getResourcePreProccessor(
        getCmsObject(), FRIENDLY_NAME_LESS_PROCESSOR);

    assertNotNull(processor);
    assertTrue(processor instanceof LessCssProcessor);
  }

  public void testSassCssProcessorLoaded() throws Exception {
    ResourcePreProcessor processor = getManager().getResourcePreProccessor(
        getCmsObject(), FRIENDLY_NAME_SASS_PROCESSOR);

    assertNotNull(processor);
    assertTrue(processor instanceof RubySassCssProcessor);
  }

  public void testCssFileCreated() throws Exception {
    // LOAD THE RESOURCE TO OPTIMIZE
    CmsResource cmsResource = getCmsObject().readResource(RESOURCE_URI);

    // OPTIMIZE IT
    String optimized = getManager().buildWebResource(getCmsObject(),
        cmsResource, CmsCssImportProcessor.FRIENDLY_NAME);

    // WRITE THE FILE
    getManager().createBuildFile(getCmsObject(), WRITE_RESOURCE_URI,
        optimized.getBytes());

    assertTrue(StringUtils.isNotBlank(optimized));
  }

  public void testLessCssProcessorExecuted() throws Exception {
    echo("Test testLessCssProcessorExecuted starting...");
    // LOAD THE RESOURCE TO OPTIMIZE
    CmsResource cmsResource = getCmsObject().readResource(
        WRITE_RESOURCE_URI);

    // OPTIMIZE IT
    String optimized = getManager().buildWebResource(getCmsObject(),
        cmsResource, FRIENDLY_NAME_LESS_PROCESSOR);

    // WRITE THE FILE
    getManager().createBuildFile(getCmsObject(), WRITE_RESOURCE_URI,
        optimized.getBytes());

    echo("Less Optimized CSS: " + optimized);
    assertTrue(StringUtils.isNotBlank(optimized));
    echo("Test testLessCssProcessorExecuted ending...");
  }

  public void testSassCssProcessorExecuted() throws Exception {
    echo("Test testSassCssProcessorExecuted starting...");

    // LOAD THE RESOURCE TO OPTIMIZE
    CmsResource cmsResource = getCmsObject()
        .readResource(SASS_RESOURCE_URI);

    // OPTIMIZE IT
    String optimized = getManager().buildWebResource(getCmsObject(),
        cmsResource, CmsCssImportProcessor.FRIENDLY_NAME);

    // WRITE THE FILE
    getManager().createBuildFile(getCmsObject(), WRITE_RESOURCE_URI,
        optimized.getBytes());

    // LOAD THE RESOURCE TO OPTIMIZE
    cmsResource = getCmsObject().readResource(WRITE_RESOURCE_URI);

    // OPTIMIZE IT
    optimized = getManager().buildWebResource(getCmsObject(), cmsResource,
        FRIENDLY_NAME_SASS_PROCESSOR);

    if (StringUtils.isBlank(optimized)) {
      // WRITE THE FILE
      getManager().createBuildFile(getCmsObject(), WRITE_RESOURCE_URI,
          optimized.getBytes());
    }

    echo("Sass Optimized CSS:\n " + optimized);
    assertTrue(StringUtils.isNotBlank(optimized));
    echo("Test testSassCssProcessorExecuted ending...");
  }

  /**
   * @return the m_manager
   */
  public CmsWebResourceOptimizerManger getManager() {
    return m_manager;
  }

  /**
   * @param m_manager
   *            the m_manager to set
   */
  public void setManager(CmsWebResourceOptimizerManger m_manager) {
    this.m_manager = m_manager;
  }

}
