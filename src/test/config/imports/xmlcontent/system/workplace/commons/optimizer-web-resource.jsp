<%@page trimDirectiveWhitespaces="true"%>
<%@ page
	import="com.dispatch.optimizer.processor.CmsWebResourceOptimizerProcessor"%>
<%
	// initialize the workplace class
	CmsWebResourceOptimizerProcessor wp = new CmsWebResourceOptimizerProcessor(
			pageContext, request, response);

	//DISPLAY THE REPORT
	wp.getCmsReport().displayReport();

	////////////////////start of switch statement 
	switch (wp.getAction()) {

	case CmsWebResourceOptimizerProcessor.ACTION_CANCEL:
		////////////////////ACTION: cancel USER DOES NOT HAVE ACCESS RIGHTS

		wp.actionCloseDialog();

		break;

	case CmsWebResourceOptimizerProcessor.ACTION_OPTIMIZE:
	case CmsWebResourceOptimizerProcessor.ACTION_DEFAULT:
	default:
		boolean wasOptimized = wp.actionBuild();
	}
%>
