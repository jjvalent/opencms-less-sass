/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.configurartion;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

/**
 * This class represents a optimizer configuration. The configuration will let
 * the system know which processors are available and the class names to load
 * for that processor
 * 
 * @author John Valentine
 * 
 */
public class CmsOptimizerConfiguration {

  /** The {@link Log} log for this class */
  private static final Log LOG = CmsLog
      .getLog(CmsOptimizerConfiguration.class);

  /** The {@link List} of {@link String} strings storing the available processors */
  private List<String> m_availableProcessorsList;

  /** The character encoding to use */
  private String m_encoding;

  /** The settings on whether or not missing imports are ignored */
  private boolean m_ignoreMissingResources;

  /**
   * The map of processor classes, key is the friendly name, value is the
   * class to load
   */
  private Map<String, String> m_processorMap;

  /**
   * Default Constructor
   */
  public CmsOptimizerConfiguration() {
    // GET A NEW ARRAY LIST
    this.m_availableProcessorsList = new ArrayList<String>();

    // GET A NEW MAP
    this.m_processorMap = new LinkedHashMap<String, String>();

    // SET THE ENCODING TO BE EMPTY
    this.m_encoding = StringUtils.EMPTY;

    // IGNORE MISSING IMPORTS IS FALSE BY DEFAULT
    this.m_ignoreMissingResources = false;
  }

  /**
   * Adds an available processor to the available processor list
   * 
   * @param avaiableProcessor
   *            the available processor to add
   */
  public void addToAvailableProcessorList(String avaiableProcessor) {
    this.getAvailableProcessorsList().add(avaiableProcessor);
  }

  /**
   * Adds the processor class to the processor map with the friendly name as
   * the key
   * 
   * @param friendlyName
   *            the name of the processor
   * @param clazz
   *            the implementating class of the processor
   */
  public void addToProcessorMap(String friendlyName, String clazz) {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Adding the friendlyName: " + friendlyName);
      LOG.debug("Adding the class: " + clazz);
    }
    getProcessorMap().put(friendlyName, clazz);

  }

  /**
   * @return the m_availableProcessorsList
   */
  public List<String> getAvailableProcessorsList() {
    return m_availableProcessorsList;
  }

  /**
   * @param m_availableProcessorsList
   *            the m_availableProcessorsList to set
   */
  public void setAvailableProcessorsList(
      List<String> m_availableProcessorsList) {
    this.m_availableProcessorsList = m_availableProcessorsList;
  }

  /**
   * @return the m_processorMap
   */
  public Map<String, String> getProcessorMap() {
    return m_processorMap;
  }

  /**
   * @param m_processorMap
   *            the m_processorMap to set
   */
  public void setProcessorMap(Map<String, String> m_processorMap) {
    this.m_processorMap = m_processorMap;
  }

  /**
   * @return the m_encoding
   */
  public String getEncoding() {
    return m_encoding;
  }

  /**
   * @param m_encoding
   *            the m_encoding to set
   */
  public void setEncoding(String m_encoding) {
    this.m_encoding = m_encoding;
  }

  /**
   * @return the m_ignoreMissingImports
   */
  public boolean isIgnoreMissingResources() {
    return m_ignoreMissingResources;
  }

  /**
   * @param m_ignoreMissingImports
   *            the m_ignoreMissingImports to set
   */
  public void setIgnoreMissingResources(boolean m_ignoreMissingResources) {
    this.m_ignoreMissingResources = m_ignoreMissingResources;
  }
}
