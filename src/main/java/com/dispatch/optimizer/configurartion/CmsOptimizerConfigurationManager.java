/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.configurartion;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.xml.sax.InputSource;

/**
 * This class loads the configuration object using a Digester
 * 
 * @author John Valentine
 * 
 */
public class CmsOptimizerConfigurationManager {

  /** The {@link Log} log for this class */
  private static final Log LOG = CmsLog
      .getLog(CmsOptimizerConfigurationManager.class);

  /** The singleton instance for this class */
  private static CmsOptimizerConfigurationManager INSTANCE;

  /** The {@link CmsOptimizerConfiguration} for this class */
  private CmsOptimizerConfiguration m_optimizerConfiguration;

  /** The path to the optimizer settings configuration XML */
  private static final String SETTINGS_FILE_PATH = "/system/modules/com.dispatch.optimizer/config/properties/optimizer-settings.xml";

  /** The path to the optimizer rules XML */
  private static final String RULES_FILE_PATH = "/system/modules/com.dispatch.optimizer/config/properties/optimizer-rules.xml";

  /**
   * Default Constructor
   */
  private CmsOptimizerConfigurationManager() {
    // NOOP
  }

  /**
   * Gets the singleton instance for this class
   * 
   * @return the singleton instance for this class
   */
  public static CmsOptimizerConfigurationManager getInstance() {
    synchronized (CmsOptimizerConfigurationManager.class) {
      if (INSTANCE == null) {
        if (LOG.isDebugEnabled()) {
          LOG.debug("Getting a new optimizer configuration instance");
        }
        INSTANCE = new CmsOptimizerConfigurationManager();

        // GET THE CONFIGURATION USING DIGESTER
        DigestTasks dTasks = new CmsOptimizerConfigurationManager().new DigestTasks();
        INSTANCE.setOptimizerConfiguration((dTasks
            .digestConfiguration()));
      }
      return INSTANCE;
    }
  }

  /**
   * Digest Tasks class to digest the optimizer settings xml file
   * 
   * @author John Valentine
   * 
   */
  private class DigestTasks {

    /**
     * Default Constructor
     */
    private DigestTasks() {
      // NOOP
    }

    /**
     * Gets the {@link CmsOptimizerConfiguration} using the settings and
     * rules file, parsed through Digester
     * 
     * @return the {@link CmsOptimizerConfiguration} that was created using
     *         Digester
     */
    private CmsOptimizerConfiguration digestConfiguration() {
      DigestTasks tasks = new DigestTasks();
      return tasks.digest();
    }

    /**
     * Digests the rules and settings files
     * 
     * @return the {@link CmsOptimizerConfiguration} from the settings file
     */
    private CmsOptimizerConfiguration digest() {
      // GET A NEW CMS OPTIMIZER CONFIGURATION
      CmsOptimizerConfiguration configuration = new CmsOptimizerConfiguration();

      try {
        // GET A CMS OBJECT TO READ FILES AS A GUEST USER
        CmsObject cms = OpenCms.initCmsObject(OpenCms.getDefaultUsers()
            .getUserGuest());

        // GET A CMS FILE FOR READING THAT HOLDS THE SETTINGS
        CmsFile settingsFile = cms.readFile(SETTINGS_FILE_PATH);

        // GET THE CONTENTS OF THE FILE
        ByteArrayInputStream settingsStream = new ByteArrayInputStream(
            settingsFile.getContents());

        // LOAD RULES FROM XML RULES FILE
        CmsFile rulesFile = cms.readFile(RULES_FILE_PATH);

        // CREATE AN INPUT SOURCE FROM THE RULES FILE
        InputSource rulesStream = new InputSource(
            new ByteArrayInputStream(rulesFile.getContents()));

        // BUILD XML DIGESTER RULES
        Digester digester = DigesterLoader.createDigester(rulesStream);

        if (LOG.isDebugEnabled()) {
          LOG.debug("Got the rules xml file!");
        }

        // PARSE USING THE DIGESTER TO GET AN
        // INSTANCE OF CMS OPTIMIZER CONFIGURATION
        configuration = (CmsOptimizerConfiguration) digester
            .parse(settingsStream);
      } catch (Exception e) {
        LOG.error("Error parsing build settings file: " + e, e);
      }
      // RETURN THE CONFIGURATION
      return configuration;
    }

  }

  /**
   * @return the m_optimizerConfiguration
   */
  public CmsOptimizerConfiguration getOptimizerConfiguration() {
    return m_optimizerConfiguration;
  }

  /**
   * @param m_optimizerConfiguration
   *            the m_optimizerConfiguration to set
   */
  public void setOptimizerConfiguration(
      CmsOptimizerConfiguration m_optimizerConfiguration) {
    this.m_optimizerConfiguration = m_optimizerConfiguration;
  }

}
