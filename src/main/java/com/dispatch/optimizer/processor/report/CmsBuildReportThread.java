/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dispatch.optimizer.processor.report;

import org.opencms.file.CmsObject;
import org.opencms.i18n.CmsMessageContainer;
import org.opencms.report.A_CmsReportThread;

/**
 * Report Thread for Web Resource Build information
 * 
 * @author John Valentine
 * 
 */
public class CmsBuildReportThread extends A_CmsReportThread {

  /**
   * @param cms
   * @param name
   */
  public CmsBuildReportThread(CmsObject cms, String name) {
    super(cms, name);

    // INITALIZE AN HTML REPORT
    initHtmlReport(cms.getRequestContext().getLocale());
  }

  /**
   * Adds the report message to the report
   * 
   * @param cmsMessageContainer
   *            the message to add to the report
   */
  public void addReportMessage(CmsMessageContainer cmsMessageContainer,
      int format) {
    // PRINT THE MESSAGE TO THE REPORT
    getReport().println(cmsMessageContainer, format);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.opencms.report.A_CmsReportThread#getReportUpdate()
   */
  @Override
  public String getReportUpdate() {
    return getReport().getReportUpdate();
  }

}
