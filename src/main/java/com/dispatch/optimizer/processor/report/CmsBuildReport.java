/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.opencms.jsp.CmsJspActionElement;
import org.opencms.report.I_CmsReportThread;
import org.opencms.workplace.list.A_CmsListReport;

/**
 * Report for Web Resource Build information
 * 
 * @author John Valentine
 * 
 */
public class CmsBuildReport extends A_CmsListReport {

  /** The name of the thread to launch as a part of this report */
  private static final String REPORT_THREAD_NAME = "web-resource-build-report";

  /** The {@link CmsBuildReportThread} for this class */
  private CmsBuildReportThread m_reportThread;

  /** The title of this report */
  public static final String REPORT_TITLE_NAME = "Web Resource Build Report For ";

  /**
   * Default Constructor
   * 
   * @param context
   *            the context to use for this report
   * @param req
   *            the request to use for this report
   * @param res
   *            the response to use for this report
   */
  public CmsBuildReport(PageContext context, HttpServletRequest req,
      HttpServletResponse res) {
    super(context, req, res);

    // SET THE TITLE
    this.setParamTitle(REPORT_TITLE_NAME + this.getParamResource());
  }

  /**
   * Constructor using a jspActionElement
   * 
   * @param jspActionElement
   *            the jspAcrionElement used to build the report
   */
  public CmsBuildReport(CmsJspActionElement jspActionElement) {
    this(jspActionElement.getJspContext(), jspActionElement.getRequest(),
        jspActionElement.getResponse());
  }

  @Override
  public I_CmsReportThread initializeThread() {
    // MAKE SURE WE DON"T OVER WRITE OUR REPORT THREAD
    if (getReportThread() == null) {

      // SET THE REPORT THREAD TO USE
      setReportThread(new CmsBuildReportThread(getCms(),
          REPORT_THREAD_NAME));
    }

    // RETURN IT
    return getReportThread();
  }

  /**
   * @return the m_reportThread
   */
  public CmsBuildReportThread getReportThread() {
    return m_reportThread;
  }

  /**
   * @param m_reportThread
   *            the m_reportThread to set
   */
  public void setReportThread(CmsBuildReportThread m_reportThread) {
    this.m_reportThread = m_reportThread;
  }

}
