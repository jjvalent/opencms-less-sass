/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResource.CmsResourceUndoMode;
import org.opencms.file.CmsResourceFilter;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.report.I_CmsReport;
import org.opencms.security.CmsPermissionSet;
import org.opencms.workplace.CmsMultiDialog;
import org.opencms.workplace.CmsWorkplaceSettings;

import ro.isdc.wro.extensions.processor.css.LessCssProcessor;
import ro.isdc.wro.extensions.processor.css.YUICssCompressorProcessor;

import com.dispatch.optimizer.processor.impl.css.CmsCssImportProcessor;
import com.dispatch.optimizer.processor.impl.image.CmsCssImageImportProcessor;
import com.dispatch.optimizer.processor.report.CmsBuildReport;
import com.dispatch.optimizer.processor.report.CmsBuildReportThread;

/**
 * Implements the build button in the context file menu for the "web-resource"
 * content type
 * 
 * * The following files use this class:
 * <ul>
 * <li>/commons/optimizer-web-resource.jsp
 * </ul>
 * <p>
 * 
 * @author John Valentine
 * 
 */
public class CmsWebResourceOptimizerProcessor extends CmsMultiDialog {

  /** The {@link Log} log for this class */
  private static final Log LOG = CmsLog
      .getLog(CmsWebResourceOptimizerProcessor.class);

  /** Value for the action: optimize. */
  public static final int ACTION_OPTIMIZE = 100101;

  /** The dialog type. */
  public static final String DIALOG_TYPE = "build";

  /** The name of the Resource Build Processors property */
  private static final String BUILD_PROCESSORS = "Resource.Optimizer.Processors";

  /** The name of the Resource Build Generated Name property */
  private static final String BUILD_GENERATED_NAME = "Resource.Optimizer.GeneratedName";

  /** The {@link CmsBuildReport} object used to build error and warning output */
  private CmsBuildReport m_cmsReport;

  /** The semi-colon delimeter used to get the processors from */
  private static final String SEMI_COLON_DELIMETER = ";";

  /** The CSS extension we are using to build a resource */
  private static final String CSS_EXTENSION = ".css";

  /** The extension for a less file, maps to use the less processor */
  protected static final String LESS_EXTENSION = "less";

  /** The extension for a scss file, maps to use the sass processor */
  protected static final String SASS_EXTENSION = "scss";

  /** The friendly name of the sass processor we are using */
  protected static final String SASS_PROCESSOR_FRIENDLY_NAME = "sassCss";

  /** The friendly name of the less processor we are using */
  protected static final String LESS_PROCESSOR_FRIENDLY_NAME = LessCssProcessor.ALIAS;

  /** The friendly name of yuiCssMin processor we are using */
  protected static final String LUI_CSS_MIN_PROCESSOR_FRIENDLY_NAME = YUICssCompressorProcessor.ALIAS;

  /**
   * Public Constructor
   * 
   * @param jsp
   *            an intialized JSP action element
   */
  public CmsWebResourceOptimizerProcessor(CmsJspActionElement jspActionElement) {
    super(jspActionElement);

    // SET THE CMS REPORT TO USE
    setCmsReport(new CmsBuildReport(jspActionElement));

    // LINK THE REPORT THREAD TOGETHER
    getCmsReport().setReportThread(
        (CmsBuildReportThread) getCmsReport().initializeThread());
  }

  /**
   * Public constructor with JSP variables.
   * <p>
   * 
   * @param context
   *            the JSP page context
   * @param req
   *            the JSP request
   * @param res
   *            the JSP response
   */
  public CmsWebResourceOptimizerProcessor(PageContext context,
      HttpServletRequest req, HttpServletResponse res) {

    this(new CmsJspActionElement(context, req, res));
  }

  /**
   * Performs the resource build/compiliation, will be called by the JSP page.
   * <p>
   * 
   * @throws JspException
   *             if problems including sub-elements occur
   */
  public boolean actionBuild() throws JspException {

    if (this.isForwarded() == false) {
      // SAVE INITIALIZED INSTANCE OF THIS CLASS IN REQUEST ATTRIBUTE FOR
      // INCLUDED SUB-ELEMENTS
      getJsp().getRequest().setAttribute(SESSION_WORKPLACE_CLASS, this);
      try {
        if (performDialogOperation()) {
          // IF NO EXCEPTION IS CAUSED AND "TRUE" IS RETURNED THE
          // OPTIMIZE
          // OPERATION WAS SUCCESSFUL
          return true;
        }
      } catch (Throwable e) {
        // ADD THE ERROR TO THE REPORT
        getCmsReport().getReportThread().addError(e);
        includeErrorpage(this, e);
      }
    }
    return false;
  }

  /**
   * @see org.opencms.workplace.CmsWorkplace#initWorkplaceRequestValues(org.opencms.workplace.CmsWorkplaceSettings,
   *      javax.servlet.http.HttpServletRequest)
   */
  protected void initWorkplaceRequestValues(CmsWorkplaceSettings settings,
      HttpServletRequest request) {

    // FILL THE PARAMETER VALUES IN THE GET/SET METHODS
    fillParamValues(request);

    // CHECK THE REQUIRED PERMISSIONS TO OPTIMIZE THE RESOURCE
    if (!checkResourcePermissions(CmsPermissionSet.ACCESS_WRITE, false)) {
      // NO WRITE PERMISSIONS FOR THE RESOURCE, SET CANCEL ACTION TO CLOSE
      // DIALOG
      setParamAction(DIALOG_CANCEL);
    }

    // SET THE DIALOG TYPE
    setParamDialogtype(DIALOG_TYPE);

    // SET THE ACTION FOR THE JSP SWITCH
    if (DIALOG_TYPE.equals(getParamAction())) {
      setAction(ACTION_OPTIMIZE);
    } else if (DIALOG_CANCEL.equals(getParamAction())) {
      setAction(ACTION_CANCEL);
    } else if (ACTION_REPORT_END == getAction()) {
      // DO NOTHING SINCE IT IS COMPLETE
    } else if ("reportend".equalsIgnoreCase(getParamAction())) {
      // DO NOTHING SINCE IT IS COMPLETE
    } else {
      // DEFAULT ACTION IS TO OPTIMIZE
      setAction(ACTION_OPTIMIZE);
    }
  }

  /**
   * Performs the resource build/compiliation.
   * <p>
   * 
   * @return true, if the resource was built, otherwise false
   * @throws CmsException
   *             if building is not successful
   */
  @Override
  protected boolean performDialogOperation() throws CmsException {

    boolean operationsSuccessful = false;

    // ON FOLDER TOUCH OR MULTI RESOURCE OPERATION DISPLAY "PLEASE WAIT"
    // SCREEN, NOT FOR SIMPLE FILE OPTIMIZE
    if (!DIALOG_WAIT.equals(getParamAction())) {
      // CHECK IF THE "PLEASE WAIT" SCREEN HAS TO BE SHOWN
      if (isMultiOperation()) {
        // SHOW PLEASE WAIT FOR EVERY MULTI RESOURCE OPERATION
        return false;
      } else {
        // CHECK IF THE SINGLE RESOURCE IS A FOLDER
        CmsResource sourceRes = getCms().readResource(
            getParamResource(), CmsResourceFilter.ALL);
        if (sourceRes.isFolder()) {
          return false;
        }
      }
    }

    // NOW OPTIMIZE THE RESOURCE(S)
    Iterator<String> i = getResourceList().iterator();
    while (i.hasNext()) {
      String resName = (String) i.next();
      try {
        if (LOG.isDebugEnabled()) {
          LOG.debug("Building single resource: " + resName);
        }
        operationsSuccessful = buildSingleResource(resName);
      } catch (CmsException cmsOptimizeWebResourceException) {
        // COLLECT EXCEPTIONS TO CREATE A DETAILED OUTPUT
        getCmsReport().getReportThread().addError(
            cmsOptimizeWebResourceException);
        addMultiOperationException(cmsOptimizeWebResourceException);
      } catch (IOException ioException) {
        // ADD THE ERROR
        getCmsReport().getReportThread().addError(ioException);
        LOG.error("Error optimizing the web resource: ", ioException);
      }
    }
    checkMultiOperationException(Messages.get(), Messages.ERR_BUILD_MULTI_0);

    return operationsSuccessful;
  }

  /**
   * Build the single resource
   * 
   * @param resourceName
   *            the resourceName(VFS File) to build
   * @return whether or not the file was built
   * @throws CmsException
   * @throws IOException
   */
  protected boolean buildSingleResource(String resourceName)
      throws CmsException, IOException {
    // LOCK RESOURCE IF AUTOLOCK IS ENABLED
    checkLock(resourceName);

    // READ THE CMS RESOURCE FROM THE VFS
    CmsResource sourceResource = getCms().readResource(resourceName,
        CmsResourceFilter.ALL);

    // ONLY BUILD IF THE RESOURCE IS A FILE
    if (sourceResource.isFile()) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Resource is a file...continuing with the build process...");
      }
      return doBuild(sourceResource);
    }
    return false;
  }

  /**
   * Build the generated file name
   * 
   * @return the generated file name
   */
  protected String buildGeneratedFileName() {
    // GET THE BASE NAME OF THE RESOURCE
    String generatedFileName = FilenameUtils.getBaseName(this
        .getParamResource());

    // GET THE FOLDER
    String folder = FilenameUtils.getFullPath(this.getParamResource());

    // UPDATE THE GENERATED FILE NAME
    generatedFileName = folder + generatedFileName + CSS_EXTENSION;

    // RETURN THE GENERATED FILE NAME
    return generatedFileName;
  }

  /**
   * Builds the list of css processors to use since none were defined
   * 
   * @return the list of csss processors to use
   */
  protected String buildOptimizeProcessorsToUse() {
    // USING THE CMS IMPORT AND CMS IMAGE IMPORT PROCESSORS BY DEFAULT
    String optimizeProcessors = CmsCssImportProcessor.FRIENDLY_NAME
        + SEMI_COLON_DELIMETER
        + CmsCssImageImportProcessor.FRIENDLY_NAME;

    // GET THE FILE EXTENSION OF THE SOURCE FILE
    String fileExtension = FilenameUtils.getExtension(this
        .getParamResource());

    // SEE WHICH PROCESSOR WE ARE USING, LESS OR SASS
    if (StringUtils.equalsIgnoreCase(LESS_EXTENSION, fileExtension)) {
      optimizeProcessors += SEMI_COLON_DELIMETER
          + LESS_PROCESSOR_FRIENDLY_NAME;
    } else if (StringUtils.equalsIgnoreCase(SASS_EXTENSION, fileExtension)) {
      optimizeProcessors += SEMI_COLON_DELIMETER
          + SASS_PROCESSOR_FRIENDLY_NAME;
    }

    // ADD THE YUI CSS MIN PROCESSOR
    optimizeProcessors += SEMI_COLON_DELIMETER
        + LUI_CSS_MIN_PROCESSOR_FRIENDLY_NAME;

    if (LOG.isDebugEnabled()) {
      LOG.debug("Built the list of processors to use: "
          + optimizeProcessors);
    }
    return optimizeProcessors;
  }

  /**
   * Performs the build/compiliation on the cmsResource
   * 
   * @param cmsResource
   *            the cmsResource to build
   * @throws CmsException
   *             in case something goes wrong
   */
  protected boolean doBuild(CmsResource cmsResource) throws CmsException,
      IOException {
    // GET THE NAMES OF THE BUILD PROCESSORS
    String optimizeProcessors = getCms().readPropertyObject(cmsResource,
        BUILD_PROCESSORS, false).getValue();

    // GET THE GENERATED FILE NAME WE ARE USING
    String generatedFileName = getCms().readPropertyObject(cmsResource,
        BUILD_GENERATED_NAME, false).getValue();

    // IF THE GENERATED FILE NAME PROPERTY IS EMPTY
    if (StringUtils.isEmpty(generatedFileName)) {

      // BUILD THE GENERATED FILE NAME
      generatedFileName = buildGeneratedFileName();

      // IF THE GENERATED FILE REFERS TO THE PARAM RESOURCE
      if (StringUtils.equalsIgnoreCase(generatedFileName,
          this.getParamResource())) {
        // PRINT ERROR
        getCmsReport()
            .getReportThread()
            .addReportMessage(
                Messages.get()
                    .container(
                        Messages.LOG_ERROR_BUILD_PROCESSORS_GENERATED_NAME,
                        generatedFileName),
                I_CmsReport.FORMAT_ERROR);

        // RETURN NOW
        return false;
      }
    }

    // IF NO PROCESSORS WERE SUPPLIED
    if (StringUtils.isBlank(optimizeProcessors)) {

      // BUILD A DEFAULT LIST OF PROCESSORS
      optimizeProcessors = buildOptimizeProcessorsToUse();
    }

    // PRINT THAT THE PROCESS HAS STARTED
    getCmsReport().getReportThread().addReportMessage(
        Messages.get().container(
            Messages.LOG_PROGRESS_BUILD_PROCESSORS_START),
        I_CmsReport.FORMAT_HEADLINE);

    if (LOG.isDebugEnabled()) {
      LOG.debug("Names of the processors to use: " + optimizeProcessors);
    }

    // SEE IF THE PROPERTIES ARE EMPTY
    if (StringUtils.isBlank(optimizeProcessors)
        || StringUtils.isBlank(generatedFileName)) {

      // PRINT THAT THE REQUIRED PROPERTIES ARE EMPTY
      getCmsReport().getReportThread().addReportMessage(
          Messages.get().container(
              Messages.LOG_ERROR_EXECUTING_BUILD_1),
          I_CmsReport.FORMAT_ERROR);
      return false;
    }

    // PRINT WHAT FILE NAME WE ARE USING
    getCmsReport().getReportThread().addReportMessage(
        Messages.get().container(
            Messages.LOG_PROGRESS_BUILD_PROCESSORS_GENERATED_NAME,
            generatedFileName), I_CmsReport.FORMAT_NOTE);

    // PRINT WHICH PROCESSORS WE ARE USING
    getCmsReport().getReportThread().addReportMessage(
        Messages.get().container(
            Messages.LOG_PROGRESS_BUILD_PROCESSORS_LIST,
            optimizeProcessors), I_CmsReport.FORMAT_NOTE);

    // SPLIT THE PROCESSOR DELIMIT ON ;
    String[] processors = optimizeProcessors.split(SEMI_COLON_DELIMETER);

    // BUILD AN EMPTY OPTIMIZED STRING
    String buildString = StringUtils.EMPTY;

    // LOOP THROUGH THE LIST OF PROCESSORS
    for (int i = 0; i < processors.length; i++) {
      try {
        // PRINT THAT THE PROCESSOR IS EXECUTING
        getCmsReport()
            .getReportThread()
            .addReportMessage(
                Messages.get()
                    .container(
                        Messages.LOG_PROGRESS_BUILD_PROCESSORS_EXECUTING,
                        processors[i]),
                I_CmsReport.FORMAT_HEADLINE);

        // BUILD IT
        buildString = CmsWebResourceOptimizerManger.getInstance()
            .buildWebResource(getCms(), cmsResource, processors[i]);

        // WRITE THE FILE
        CmsWebResourceOptimizerManger.getInstance().createBuildFile(
            getCms(), generatedFileName, buildString.getBytes());

        // UPDATE THE RESOURCE TO OPTIMIZE
        cmsResource = getCms().readResource(generatedFileName);

        // PRINT THAT THE PROCESSOR IS DONE
        getCmsReport().getReportThread().addReportMessage(
            Messages.get().container(
                Messages.LOG_PROGRESS_BUILD_PROCESSORS_DONE,
                processors[i]), I_CmsReport.FORMAT_OK);
      } catch (Exception webResourceProcessorException) {
        // PRINT THE EXCEPTION TO THE REPORT
        getCmsReport().getReportThread().addReportMessage(
            Messages.get().container(
                Messages.LOG_ERROR_BUILD_OPTIMIZE_0,
                processors[1],
                webResourceProcessorException
                    .getLocalizedMessage()),
            I_CmsReport.FORMAT_ERROR);

        LOG.error("Error processing the resource: ",
            webResourceProcessorException);

        // UNDO CHANGES TO THE FILE
        undoChanges(generatedFileName);

        // RETURN NOW
        return false;
      }
    }
    return true;
  }

  /**
   * Reverts the file to the version that is on the online project(published)
   * 
   * @param fileName
   *            the fileName to revert
   */
  protected void undoChanges(String fileName) {
    try {
      // LOCK THE RESOURCE
      getCms().lockResource(fileName);

      // UNDO THE CHANGES
      getCms().undoChanges(fileName,
          CmsResourceUndoMode.MODE_UNDO_CONTENT);

      // UNLOCK THE RESOURCE
      getCms().unlockResource(fileName);
    } catch (Exception undoException) {
      getCmsReport().getReportThread().addError(undoException);
      LOG.error("Error restoring the file from the online project: ",
          undoException);
    }
  }

  /**
   * @return the m_cmsReport
   */
  public CmsBuildReport getCmsReport() {
    return m_cmsReport;
  }

  /**
   * @param m_cmsReport
   *            the m_cmsReport to set
   */
  public void setCmsReport(CmsBuildReport m_cmsReport) {
    this.m_cmsReport = m_cmsReport;
  }
}
