/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor;

import org.opencms.i18n.A_CmsMessageBundle;
import org.opencms.i18n.I_CmsMessageBundle;

/**
 * Convenience class to access the localized messages of this OpenCms package.
 * <p>
 * 
 * @author John Valentine
 * 
 * @version $Revision: 1.0 $
 * 
 * @since 8.0.3
 */
public final class Messages extends A_CmsMessageBundle {

  /** Message constant for key in the resource bundle. */
  public static final String LOG_PROGRESS_BUILD_PROCESSORS_START = "LOG_PROGRESS_BUILD_PROCESSORS_START";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_PROGRESS_BUILD_PROCESSORS_GENERATED_NAME = "LOG_PROGRESS_BUILD_PROCESSORS_GENERATED_NAME";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_PROGRESS_BUILD_PROCESSORS_LIST = "LOG_PROGRESS_BUILD_PROCESSORS_LIST";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_PROGRESS_BUILD_PROCESSORS_EXECUTING = "LOG_PROGRESS_BUILD_PROCESSORS_EXECUTING";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_PROGRESS_BUILD_PROCESSORS_DONE = "LOG_PROGRESS_BUILD_PROCESSORS_DONE";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_ERROR_BUILD_PROCESSORS = "LOG_ERROR_BUILD_PROCESSORS";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_ERROR_BUILD_PROCESSORS_GENERATED_NAME = "LOG_ERROR_BUILD_PROCESSORS_GENERATED_NAME";

  /** Message constant for key in the resource bundle. */
  public static final String ERR_BUILD_MULTI_0 = "ERR_BUILD_MULTI_0";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_ERROR_BUILD_OPTIMIZE_0 = "LOG_ERROR_BUILD_OPTIMIZE_0";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_ERROR_EXECUTING_BUILD_0 = "LOG_ERROR_EXECUTING_BUILD_0";

  /** Message constant for key in the resource bundle. */
  public static final String GUI_EXPLORER_CONTEXT_BUILD_0 = "GUI_EXPLORER_CONTEXT_BUILD_0";

  /** Message constant for key in the resource bundle. */
  public static final String LOG_ERROR_EXECUTING_BUILD_1 = "LOG_ERROR_EXECUTING_BUILD_1";

  /** Name of the used resource bundle. */
  private static final String BUNDLE_NAME = "com.dispatch.optimizer.processor.messages";

  /** Static instance member. */
  private static final I_CmsMessageBundle INSTANCE = new Messages();

  /**
   * Hides the public constructor for this utility class.
   * <p>
   */
  private Messages() {

    // hide the constructor
  }

  /**
   * Returns an instance of this localized message accessor.
   * <p>
   * 
   * @return an instance of this localized message accessor
   */
  public static I_CmsMessageBundle get() {

    return INSTANCE;
  }

  /**
   * Returns the bundle name for this OpenCms package.
   * <p>
   * 
   * @return the bundle name for this OpenCms package
   */
  public String getBundleName() {

    return BUNDLE_NAME;
  }

}
