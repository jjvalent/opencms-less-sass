/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsVfsResourceNotFoundException;
import org.opencms.file.types.CmsResourceTypePlain;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.model.group.processor.Injector;
import ro.isdc.wro.model.group.processor.InjectorBuilder;
import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;

import com.dispatch.optimizer.configurartion.CmsOptimizerConfiguration;
import com.dispatch.optimizer.configurartion.CmsOptimizerConfigurationManager;
import com.dispatch.optimizer.processor.impl.css.CmsCssImportProcessor;
import com.dispatch.optimizer.processor.impl.image.CmsCssImageImportProcessor;

/***
 * This class represents the CmsWebResourceOptimizerManager
 * 
 * The manager will load the processor from the classpath, inject configurations
 * needed and return the compiled string
 * 
 * @author John Valentine
 * 
 */
public class CmsWebResourceOptimizerManger {

  /** The {@link Log} log for this class */
  private static final Log LOG = CmsLog
      .getLog(CmsWebResourceOptimizerManger.class);

  /** The singleton instance for this class */
  private static CmsWebResourceOptimizerManger INSTANCE;

  /** The {@link CmsOptimizerConfiguration} for this class */
  private CmsOptimizerConfiguration m_optimizerConfiguration;

  /**
   * Default Constructor
   */
  private CmsWebResourceOptimizerManger() {
    if (LOG.isDebugEnabled()) {
      LOG.debug("CmsWebResourceOptimizerManager constructor was called");
    }

    // SET THE OPTIMIZER CONFIGURATION
    setOptimizerConfiguration(CmsOptimizerConfigurationManager
        .getInstance().getOptimizerConfiguration());
  }

  /**
   * Gets the singleton instance for this class
   * 
   * @return the singleton instance for this class
   */
  public static CmsWebResourceOptimizerManger getInstance() {
    synchronized (CmsWebResourceOptimizerManger.class) {
      if (INSTANCE == null) {
        INSTANCE = new CmsWebResourceOptimizerManger();
      }
      return INSTANCE;
    }
  }

  /**
   * Returns a ResourcePreProcessor
   * 
   * @param cmsObject
   *            the cmsObject to use to build the pre processor
   * @param nameOfPreprocessor
   *            the friendly name of the pre processor
   * @return the ResourcePreProcessor instance
   */
  protected ResourcePreProcessor getResourcePreProccessor(
      CmsObject cmsObject, String nameOfPreprocessor) {

    // BUILD A WRO CONFIGURATION FOR THE PRE PROCESSOR
    final WroConfiguration wroConfiguration = new WroConfiguration();

    // SET UP THE OPTIONS FOR THE CONFIG
    wroConfiguration.setIgnoreFailingProcessor(getOptimizerConfiguration()
        .isIgnoreMissingResources());
    wroConfiguration.setEncoding(getOptimizerConfiguration().getEncoding());

    // SET THE CONTEXT AS STANDALONE
    Context.set(Context.standaloneContext(), wroConfiguration);

    // CREATE INJECTOR WHICH WILL INJECT ALL DEPENDENCIES OF THE PROCESSOR
    Injector injector = new InjectorBuilder().build();

    // GET A NULL PROCESSOR
    ResourcePreProcessor processor = null;

    // SEE IF WE ARE USING THE OPEN CMS IMPORT PROCESSOR
    if (StringUtils.equalsIgnoreCase(nameOfPreprocessor,
        CmsCssImportProcessor.FRIENDLY_NAME)) {

      // CREATE A NEW ONE
      processor = new CmsCssImportProcessor(cmsObject);

      // SEE IF WE ARE USING THE OPEN CMS IMAGE IMPORT PROCESSOR
    } else if (StringUtils.equalsIgnoreCase(nameOfPreprocessor,
        CmsCssImageImportProcessor.FRIENDLY_NAME)) {

      // CREATE A NEW CMS CSS IMAGE PROCESSOR
      processor = new CmsCssImageImportProcessor(cmsObject);
    } else {

      // GET THE CANNOCIAL NAME OF THE CLASS
      String nameOfProcessorClass = getOptimizerConfiguration()
          .getProcessorMap().get(nameOfPreprocessor);

      try {

        // FIND THE CLASS IN THE CLASS LOADER
        Class<?> resourceProcessorClass = this.getClass()
            .getClassLoader().loadClass(nameOfProcessorClass);

        // CREATE A NEW ONE
        processor = (ResourcePreProcessor) resourceProcessorClass
            .newInstance();
      } catch (Exception classLoaderException) {
        LOG.error("Error loading the class from the class loader: ",
            classLoaderException);
      }
    }

    // INJECT ALL REQUIRED FIELDS
    injector.inject(processor);

    return processor;
  }

  /**
   * Executes the processor defined by the processorFriendlyName and returns
   * the generated String representation of the build/compiliation
   * 
   * @param cmsObject
   *            the cmsObject used to read cmsResource
   * @param cmsResource
   *            the cmsResource to read
   * @param processorFriendlyName
   *            the proccessorFriendlyName to use for compiliation
   * @return the generated string representation of the build
   * @throws CmsException
   * @throws IOException
   */
  public String buildWebResource(CmsObject cmsObject,
      CmsResource cmsResource, String processorFriendlyName)
      throws CmsException, IOException, CmsVfsResourceNotFoundException {

    // BUILD THE RESOURCE USING THE SITE PATH
    final Resource resource = Resource.create(cmsObject.getRequestContext()
        .getSitePath(cmsResource), ResourceType.CSS);

    // READ THE CONTENTS OF THE CMS RESOURCE
    final Reader reader = new StringReader(new String(cmsObject.readFile(
        cmsResource).getContents()));

    // GET A STRING WRITER OBJECT
    StringWriter stringWriter = new StringWriter();

    // GET THE PREPROCESSOR
    ResourcePreProcessor processor = this.getResourcePreProccessor(
        cmsObject, processorFriendlyName);

    // EXECUTE THE PROCESSOR
    processor.process(resource, reader, stringWriter);

    // RETURN THE GENERATED STRING
    return stringWriter.toString();
  }

  /**
   * Creates the file in the OpenCms for further use
   * 
   * @param cmsObject
   *            the cmsObject used to create the file
   * @param fileName
   *            the fileName to use
   * @param content
   *            the content to write
   * @return whether or not the file was created
   */
  public boolean createBuildFile(CmsObject cmsObject, String fileName,
      byte[] content) throws CmsException {

    // GET A CMS FILE OBJECT
    CmsFile createdFile = null;

    // IF THE FILE EXISTS DOES NOT EXIST
    if (cmsObject.existsResource(fileName) == false) {

      // CREATE THE PLAIN RESOURCE
      createdFile = cmsObject.readFile(cmsObject.createResource(fileName,
          CmsResourceTypePlain.getStaticTypeId(), content,
          new ArrayList<CmsProperty>()));
    } else {

      // RESOURCE EXISTS, LOCK THE FILE
      cmsObject.lockResource(fileName);

      // READ THE FILE TO GET A CMS FILE OBJECT
      createdFile = cmsObject.readFile(fileName);

      // SET THE CONTENTS TO THE FILE BYTE CONTENT
      createdFile.setContents(content);

      // WRITE THE CMS FILE
      cmsObject.writeFile(createdFile);
    }

    // UNLOCK IT
    cmsObject.unlockResource(createdFile);

    return true;
  }

  /**
   * @return the m_optimizerConfiguration
   */
  protected CmsOptimizerConfiguration getOptimizerConfiguration() {
    return m_optimizerConfiguration;
  }

  /**
   * @param m_optimizerConfiguration
   *            the m_optimizerConfiguration to set
   */
  protected void setOptimizerConfiguration(
      CmsOptimizerConfiguration m_optimizerConfiguration) {
    this.m_optimizerConfiguration = m_optimizerConfiguration;
  }

}
