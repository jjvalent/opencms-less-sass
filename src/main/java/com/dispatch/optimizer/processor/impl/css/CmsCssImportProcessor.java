/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dispatch.optimizer.processor.impl.css;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsVfsResourceNotFoundException;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;
import ro.isdc.wro.model.resource.SupportedResourceType;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;
import ro.isdc.wro.util.WroUtil;

import com.dispatch.optimizer.configurartion.CmsOptimizerConfiguration;
import com.dispatch.optimizer.configurartion.CmsOptimizerConfigurationManager;

/**
 * CmsCssImport Processor responsible for handling css <code>@import</code>
 * statement. It is implemented as both: preProcessor & postProcessor. It is
 * necessary because preProcessor is responsible for updating model with found
 * imported resources, while post processor removes import occurrences.
 * <p/>
 * When processor finds an import which is not valid, it will check the
 * {@link CmsOptimizerConfiguration#isIgnoreMissingResources()} flag. If it is
 * set to false, the processor will fail.
 * 
 * @author Alex Objelean
 * @author John Valentine
 */
@SupportedResourceType(ResourceType.CSS)
public class CmsCssImportProcessor implements ResourcePreProcessor {

  /** The {@link Log} log for this class */
  private static final Log LOG = CmsLog.getLog(CmsCssImportProcessor.class);

  /** The {@link CmsObject} used to locate and read content for the resource */
  private CmsObject m_cmsObject;

  /** The {@link CmsOptimizerConfiguration} for this class */
  private CmsOptimizerConfiguration m_buildConfiguration;

  /** The friendly name for this class */
  public static final String FRIENDLY_NAME = "cmsCssImport";

  /**
   * List of processed resources, useful for detecting deep recursion. A
   * {@link ThreadLocal} is used to ensure that the processor is thread-safe
   * and doesn't eroneously detect recursion when running in a concurrent
   * environment.
   */
  private final ThreadLocal<List<String>> m_processedImports = new ThreadLocal<List<String>>() {
    @Override
    protected List<String> initialValue() {
      return new ArrayList<String>();
    };
  };

  /** The pattern to find cssImport declarations */
  private static final Pattern PATTERN = Pattern.compile(WroUtil
      .loadRegexpWithKey("cssImport"));

  /** Gets the regex to load cssImports from comments */
  private static final String REGEX_IMPORT_FROM_COMMENTS = WroUtil
      .loadRegexpWithKey("cssImportFromComments");

  /**
   * Default Constructor is hidden
   */
  private CmsCssImportProcessor() {

    // SET THE BUILD CONFIGURATION FOR THIS CLASS
    setBuildConfiguration(CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration());
  }

  /**
   * Constructor with a {@link CmsObject} used to find and read resources
   * within the VFS
   * 
   * @param cmsObject
   *            the cmsObject to use to find and read resources
   */
  public CmsCssImportProcessor(CmsObject cmsObject) {
    // CALL THE HIDDEN CONSTRUCTOR
    this();

    // SET THE CMS OBJECT
    this.setCmsObject(cmsObject);
  }

  /**
   * {@inheritDoc}
   */
  public void process(Resource resource, Reader reader, Writer writer)
      throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Applying {} processor "
          + CmsCssImportProcessor.this.getClass().getSimpleName());
    }

    try {
      final String result = parseCss(resource, reader);
      writer.write(result);

      if (LOG.isDebugEnabled()) {
        LOG.debug("Imports Generated CSS: " + result);
      }
      getProcessedList().clear();
    } catch (CmsException cmsException) {
      if (!getBuildConfiguration().isIgnoreMissingResources()) {
        // GET AN IO EXCEPTION FROM THE CMS EXCEPTION
        IOException ioException = new IOException(cmsException);

        // LOG IT TO THE ERROR LOG
        LOG.error("File was not found in the OpenCms System: ",
            cmsException);

        // THROW THE EXCEPTION
        throw ioException;
      }

    } finally {
      reader.close();
      writer.close();
    }

  }

  /**
   * @return the m_processedImports
   */
  private List<String> getProcessedList() {
    return m_processedImports.get();
  }

  /**
   * Parses the css and returns the string contents of the css
   * 
   * @param resource
   *            {@link Resource} to process.
   * @param reader
   *            Reader for processed resource.
   * @return css content with all imports processed.
   * 
   */
  private String parseCss(final Resource resource, final Reader reader)
      throws IOException, CmsException, CmsVfsResourceNotFoundException {
    if (getProcessedList().contains(resource.getUri())) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("[WARN] Recursive import detected: {} " + resource);
      }
      onRecursiveImportDetected();
      return StringUtils.EMPTY;
    }
    getProcessedList().add(resource.getUri());
    final StringBuffer sb = new StringBuffer();
    final List<Resource> importsCollector = getImportedResources(resource);

    if (!importsCollector.isEmpty()) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Imported resources found : {} "
            + importsCollector.size());
      }
    }
    sb.append(IOUtils.toString(reader)).append("\n");
    if (LOG.isDebugEnabled()) {
      LOG.debug("importsCollector: {} " + importsCollector);
    }

    // LOOP THROUGH THE IMPORTED RESOURCES
    for (Resource importResource : importsCollector) {

      // GET A READER OBJECT
      Reader importReader = new StringReader(new String(getCmsObject()
          .readFile(importResource.getUri()).getContents(),
          getBuildConfiguration().getEncoding()));

      try {

        // APPPEND THE CONTENTS OF THE FILE
        sb.append(parseCss(importResource, importReader)).append("\n");

      } catch (Exception parseCssException) {

        // IF WE ARE NOT IGNORING MISSING RESOURCES
        if (!getBuildConfiguration().isIgnoreMissingResources()) {

          // GET AN IO EXCEPTION FROM THE CMS EXCEPTION
          IOException ioException = new IOException(parseCssException);

          // LOG IT TO THE ERROR LOG
          LOG.error("There was an error while parsing the CSS: ",
              parseCssException);

          // THROW THE EXCEPTION
          throw ioException;
        }

        if (LOG.isDebugEnabled()) {
          LOG.debug("Ignore missing resources is true, continuing...");
        }
        continue;
      }
    }

    return removeImportStatements(sb.toString());
  }

  /**
   * Invoked when a recursive import is detected. Used to assert the recursive
   * import detection correct behavior. By default this method does nothing.
   * 
   * @VisibleForTesting
   */
  protected void onRecursiveImportDetected() {
  }

  /**
   * Removes all @import statements for css.
   * 
   * @parma content the string to remove @import statements from
   */
  private String removeImportStatements(final String content) {
    final Matcher matcher = PATTERN.matcher(content);
    final StringBuffer stringBuffer = new StringBuffer();
    while (matcher.find()) {
      // add and check if already exist
      matcher.appendReplacement(stringBuffer, StringUtils.EMPTY);
    }
    matcher.appendTail(stringBuffer);
    return stringBuffer.toString();
  }

  /**
   * Find a set of imported resources inside a given resource.
   */
  private List<Resource> getImportedResources(final Resource resource)
      throws IOException, CmsException {
    // it should be sorted
    final List<Resource> imports = new ArrayList<Resource>();
    String css = EMPTY;
    try {
      // GET THE LOCATION OF THE RESOURCE, AKA URI
      String locationUri = resource.getUri();

      if (LOG.isDebugEnabled()) {
        LOG.debug("Finding resource by its uri from OpenCms: "
            + locationUri);
      }
      // GET A CMS FILE FOR THE RESOURCE
      CmsFile resourceFile = getCmsObject().readFile(locationUri);

      // GET AN IPUT STREAM ON THE BYTE ARRAY
      InputStream byteInputStream = new ByteArrayInputStream(
          resourceFile.getContents());

      // GET THE CSS CONTENTS
      css = IOUtils.toString(byteInputStream, getBuildConfiguration()
          .getEncoding());
    } catch (IOException e) {
      if (!getBuildConfiguration().isIgnoreMissingResources()) {
        LOG.error("Invalid import detected: {}" + resource.getUri());
        throw e;
      }
    } catch (CmsException e) {
      if (!getBuildConfiguration().isIgnoreMissingResources()) {
        LOG.error("File was not found in the OpenCms VFS, Invalid import detected: {}"
            + resource.getUri());
        IOException ioException = new IOException(e);
        throw ioException;
      }
    }
    // REMOVE IMPORTS FROM COMMENTS BEFORE PARSE THE FILE
    css = css.replaceAll(REGEX_IMPORT_FROM_COMMENTS, StringUtils.EMPTY);
    final Matcher m = PATTERN.matcher(css);
    while (m.find()) {
      final Resource importedResource = buildImportedResource(
          resource.getUri(), m.group(1));
      // check if already exist
      if (imports.contains(importedResource)) {
        if (LOG.isDebugEnabled()) {
          LOG.debug("[WARN] Duplicate imported resource: {} "
              + importedResource);
        }
      } else {
        imports.add(importedResource);
      }
    }
    return imports;
  }

  /**
   * Build a {@link Resource} object from a found importedResource inside a
   * given resource.
   */

  private Resource buildImportedResource(final String resourceUri,
      final String importUrl) {
    final String absoluteUrl = computeAbsoluteUrl(resourceUri, importUrl);
    return Resource.create(absoluteUrl, ResourceType.CSS);
  }

  /**
   * Computes absolute url of the imported resource.
   * 
   * @param relativeResourceUri
   *            uri of the resource containing the import statement.
   * @param importUrl
   *            found import url.
   * @return absolute url of the resource to import.
   */
  private String computeAbsoluteUrl(final String relativeResourceUri,
      final String importUrl) {
    final String folder = FilenameUtils.getFullPath(relativeResourceUri);

    if (LOG.isDebugEnabled()) {
      LOG.debug("Looking at folder: " + folder);
      LOG.debug("Relative Resource Uri: " + relativeResourceUri);
      LOG.debug("ImportUrl: " + importUrl);
    }

    String absoluteImportUrl = StringUtils.EMPTY;
    if (StringUtils.startsWith(importUrl, "/")) {
      absoluteImportUrl = importUrl;
    } else {
      // remove '../' & normalize the path.
      absoluteImportUrl = ro.isdc.wro.util.StringUtils.cleanPath(folder
          + importUrl);
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug("Absolute Import URL: " + absoluteImportUrl);
    }
    return absoluteImportUrl;
  }

  /**
   * @return the m_cmsObject
   */
  public CmsObject getCmsObject() {
    return m_cmsObject;
  }

  /**
   * @param m_cmsObject
   *            the m_cmsObject to set
   */
  public void setCmsObject(CmsObject m_cmsObject) {
    this.m_cmsObject = m_cmsObject;
  }

  /**
   * @return the m_buildConfiguration
   */
  protected CmsOptimizerConfiguration getBuildConfiguration() {
    return m_buildConfiguration;
  }

  /**
   * @param m_buildConfiguration
   *            the m_buildConfiguration to set
   */
  protected void setBuildConfiguration(
      CmsOptimizerConfiguration m_buildConfiguration) {
    this.m_buildConfiguration = m_buildConfiguration;
  }
}
