/**
 *    This file is part of Web Resource Optimizer Module.
 *
 *    Web Resource Optimizer Module is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Web Resource Optimizer Module is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Web Resource Optimizer Module.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dispatch.optimizer.processor.impl.image;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsVfsResourceNotFoundException;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;
import ro.isdc.wro.model.resource.SupportedResourceType;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;

import com.dispatch.optimizer.configurartion.CmsOptimizerConfiguration;
import com.dispatch.optimizer.configurartion.CmsOptimizerConfigurationManager;

/**
 ** 
 * CmsCssImageImport Processor responsible for handling css image
 * <code>url</code> statement. It is implemented as both: preProcessor &
 * postProcessor. It is necessary because preProcessor is responsible for
 * updating model with found image imported resources, while post processor
 * updates the image url to a relative path within the OpenCms context
 * 
 * {@link CmsOptimizerConfiguration#isIgnoreMissingResources()} flag. If it is
 * set to false, the processor will fail.
 * 
 * @author John Valentine
 */
@SupportedResourceType(ResourceType.CSS)
public class CmsCssImageImportProcessor implements ResourcePreProcessor {

  /** Regex that identifies image url's inside css files */
  private static final String IMPORT_IMAGE_REGEX = "url\\s*\\(\\s*[\\'\"\\s](\\S*\\.(?:jpe?g|gif|png))[\'\"\\s]*\\)?";

  /** The {@link Log} log for this class */
  private static final Log LOG = CmsLog
      .getLog(CmsCssImageImportProcessor.class);

  /** The {@link CmsObject} used to locate and read content for the resource */
  private CmsObject m_cmsObject;

  /** The {@link CmsOptimizerConfiguration} for this class */
  private CmsOptimizerConfiguration m_buildConfiguration;

  /** The friendly name for this class */
  public static final String FRIENDLY_NAME = "cmsCssImageImport";

  /** The beginning of the url for an image url */
  private static final String IMAGE_URL_BEGINNING = "url(\"";

  /** The ending of the url for an image url */
  private static final String IMAGE_URL_ENDING = "\")";

  /** The pattern to find image import declarations */
  private static final Pattern PATTERN = Pattern.compile(IMPORT_IMAGE_REGEX,
      Pattern.CASE_INSENSITIVE);

  /**
   * Default Constructor is hidden
   */
  private CmsCssImageImportProcessor() {

    // SET THE BUILD CONFIGURATION FOR THIS CLASS
    setBuildConfiguration(CmsOptimizerConfigurationManager.getInstance()
        .getOptimizerConfiguration());
  }

  /**
   * Constructor with a {@link CmsObject} used to build image resource links
   * for the online project
   * 
   * @param cmsObject
   *            the cmsObject to use to build the image resource link in the
   *            online project
   */
  public CmsCssImageImportProcessor(CmsObject cmsObject) {
    // CALL THE HIDDEN CONSTRUCTOR
    this();

    // SET THE CMS OBJECT
    this.setCmsObject(cmsObject);
  }

  /**
   * {@inheritDoc}
   */
  public void process(Resource resource, Reader reader, Writer writer)
      throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Applying {} processor "
          + CmsCssImageImportProcessor.this.getClass()
              .getSimpleName());
    }

    try {
      // GET THE UPDATED IMAGE LINKS
      final String result = parseCss(resource, reader);

      // WRITE IT TO THE WRITER
      writer.write(result);

      if (LOG.isDebugEnabled()) {
        LOG.debug("Imports Generated CSS: " + result);
      }
    } catch (CmsException cmsException) {
      if (!getBuildConfiguration().isIgnoreMissingResources()) {
        // GET AN IO EXCEPTION FROM THE CMS EXCEPTION
        IOException ioException = new IOException(cmsException);

        // LOG IT TO THE ERROR LOG
        LOG.error("Image File was not found in the OpenCms System: ",
            cmsException);

        // THROW THE EXCEPTION
        throw ioException;
      }

    } finally {
      reader.close();
      writer.close();
    }

  }

  /**
   * Parses the css and returns the string contents of the css with the
   * updated image links
   * 
   * @param resource
   *            {@link Resource} to process.
   * @param reader
   *            Reader for processed resource.
   * @return css content with all image links processed.
   * 
   */
  private String parseCss(final Resource resource, final Reader reader)
      throws IOException, CmsException, CmsVfsResourceNotFoundException {

    // BUILD AN EMPTY STRING BUFFER
    final StringBuffer sb = new StringBuffer();

    // GET THE UPDATED IMAGE IMPORTS
    sb.append(getImportedResources(resource));

    // RETURN IT
    return sb.toString();
  }

  /**
   * Find a set of imported resources inside a given resource and replaces the
   * image link.
   */
  private String getImportedResources(final Resource resource)
      throws IOException, CmsException {

    // BUILD AN EMPTY IMAGE RESOURCE LINK
    String imageResourceLink = EMPTY;
    try {
      // GET THE LOCATION OF THE RESOURCE, AKA URI
      String locationUri = resource.getUri();

      if (LOG.isDebugEnabled()) {
        LOG.debug("Finding images in the css resource by its uri from OpenCms: "
            + locationUri);
      }

      // GET A CMS FILE FOR THE RESOURCE
      CmsFile resourceFile = getCmsObject().readFile(locationUri);

      // GET AN IPUT STREAM ON THE BYTE ARRAY
      InputStream byteInputStream = new ByteArrayInputStream(
          resourceFile.getContents());

      // GET THE CONTENTS IN THE FILE AS A STRING
      String contentString = IOUtils.toString(byteInputStream);

      // FIND IMAGE REFERENCES
      final Matcher matcher = PATTERN.matcher(contentString);

      // BUILD AN EMPTY STRING BUFFER
      final StringBuffer stringBuffer = new StringBuffer();

      // APPEND A NEWLINE TO THE STRING
      stringBuffer.append("\n");

      // WHILE THERE ARE MATCHES FOUND
      while (matcher.find()) {

        // GET THE MATCHED STRING
        String matched = matcher.group(1);

        if (LOG.isDebugEnabled()) {
          LOG.debug("Build image relative link: " + matched);
        }

        // GET THE IMAGE RELATIVE LINK
        imageResourceLink = IMAGE_URL_BEGINNING
            + OpenCms.getLinkManager()
                .substituteLinkForUnknownTarget(getCmsObject(),
                    matched) + IMAGE_URL_ENDING;

        // REPLACE THE IMAGE LINK
        matcher.appendReplacement(stringBuffer, imageResourceLink);
      }

      // APPEND THE REST OF THE CONTENT STRING
      matcher.appendTail(stringBuffer);

      if (LOG.isDebugEnabled()) {
        LOG.debug("Updated Content for image import: "
            + stringBuffer.toString());
      }

      // RETURN THE UPDATED CONTENT STRING
      return stringBuffer.toString();
    } catch (Exception imageResourceNotFoundException) {
      if (!getBuildConfiguration().isIgnoreMissingResources()) {
        LOG.error("File was not found in the OpenCms VFS, Invalid image import detected: {}"
            + resource.getUri());
        IOException ioException = new IOException(
            imageResourceNotFoundException);
        throw ioException;
      }
    }
    return StringUtils.EMPTY;
  }

  /**
   * @return the m_cmsObject
   */
  public CmsObject getCmsObject() {
    return m_cmsObject;
  }

  /**
   * @param m_cmsObject
   *            the m_cmsObject to set
   */
  public void setCmsObject(CmsObject m_cmsObject) {
    this.m_cmsObject = m_cmsObject;
  }

  /**
   * @return the m_buildConfiguration
   */
  protected CmsOptimizerConfiguration getBuildConfiguration() {
    return m_buildConfiguration;
  }

  /**
   * @param m_buildConfiguration
   *            the m_buildConfiguration to set
   */
  protected void setBuildConfiguration(
      CmsOptimizerConfiguration m_buildConfiguration) {
    this.m_buildConfiguration = m_buildConfiguration;
  }
}
